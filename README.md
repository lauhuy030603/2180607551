# 2180607551
| Tiltle | library manager - look up the location of a book so I can find it quickly in the library.  |
| ---------- | ---------- |
| **Value Statement** | As a library patron,I want to be able to easily find the location of a bookso I can find it quickly in the library.  |
| **Acceptance criteria** |**Scenario1** <p> Given there are multiple books with the same title and author in the library collection.<p>When I search for the book by its title and author.<p>Then the system should provide me with a list of locations (shelf numbers or sections) where the books are located in the library.<p>**Scenario2**<p>Given the library system has a large collection of books.<p>When I search for the book.<p>Then the system should respond promptly and accurately with the book's location information.|
| **Definition of Done** | *Unit Tests Passed<p>*Acceptance Criteria Met<p>*Code Reviewed<p>*Functional Tests Passed<p>*Non - Functional Requirements Met<p>*Product Owner Accepcts User Story |
|**owner**|Huy |
|**Interation**| |
|**Estimate**| 5Poitn|


![Git](/uploads/7e815e8a8b13451ee729a7fb7c488d16/Git.png)

| **Title**             | Manage book warehouse|
|-------------------|----------------------|
| **Value Statement** | As a library manager, I want to manage libraries at school and company, so I can manage books more easily.|
|**Acceptance Criteria** |<p>**Acceptance Criteria 1:** I want to be able to add new books to the library's inventory so that I can keep track of all the books available.<p>**Acceptance Criteria 2:** I want to be able to update the information about a book in the inventory so that I can keep it accurate.<p>**Acceptance Criteria 3:** I want to be able to delete books from the inventory so that I can remove books that are no longer available.<p>**Acceptance Criteria 4:** I want to be able to search the inventory by title, author, or subject so that I can quickly find the books I need. <p>**Acceptance Criteria 5:** I want to be able to view a list of all the books in the inventory so that I can get an overview of the library's collection.|
|**Definition of Done** | <p> I can enter the title, author, subject, and number of copies of the new book.<p> The new book is added to the inventory.<p> I can view the new book in the inventory. |
| **Owner** | VAN HUY |
| **Ieration** |Unscheduled |
| **Estimate** | 3 Points |



# Ten: Truong Thanh Dat ##
MSSV: 218067432 

Lop : 21DTHD2 


| Title                  | Manager borrows and returns books  |
| ---------------------- | ---------------------------------- |
| **Value Statment**     | As a library manager, I want to manage readers' borrowing and returning books, so that I can inventory and manage books in more detail.                   |
| **Acceptance Criteria**|<ins>**_Acceptance Criteria 1_**</ins>:<br> + Given the reader has selected a book to borrow and that book is available in the library . <br> + When the library manager enters reader and book information into the system, including loan date and expiration date.<br> + Then the system provides information about expiration dates and rules for borrowing books to readers.<p> <br> <ins> **_Acceptance Criteria 2_**</ins>:<br> + Given is data about borrowing and returning books available in the system.<br> +  When the library manager creates reports and statistics about book borrowing and returning.<br> +  Then the system displays reports and statistics, including the number of books borrowed, the number of times the book is late, and the levels of book usage over time.                       |
|  **Defintion of Done**                    |  +   Check out book <br> +  Manage reader information <br> +  Report  <br> + Test Case Complete                                 |
|**Owner**|Dat|
|**Iteration**| Unscheduled|
|**Estimate**| 3 point|


![WDF](/uploads/f27dd364372a7427a4e7a3526049883f/WDF.png)


# 2180608272 / 21DTHD2

| **Function**                      | **Describe** |
|-----------------------------------|-----------------------------------------|
| **Name**                          | Library management software - book lookup 
| **Value Statement**               | As a manager, I want to easily find and access information about books in the online library based on many different criteria, so that I can efficiently search for books. 
| **Acceptance Criteria**           |  GIVEN a user is on the main page of the application,<br> WHEN the user wants to search for books by the criteria "Title,".<br> THEN the system should allow the user to enter a search keyword for the book's title.<br> WHEN the user submits the search with a keyword,<br> THEN, the system will display a list of books that match the search criteria, displaying information including book code, book name, author, year of publication.<br> WHEN the user clicks on a book in the search results,<br> THEN the system should show detailed information about that book.|
| **Definition of Done**            |  The user interface has been developed to display the book lookup page and search results.<br> Search by "Title" criteria has been implemented and works correctly.<br> Search results display book information in an appropriate and easy-to-read format.<br> Users can view book details and return to the results list easily. |
| **Owner**                         | Nguyễn Phước Duy_2180608272 |
| **Iteration**                     | The book lookup function is expected to be implemented in the 5th part of the library management application development project. |


![Screenshot_2023-10-12_143233](/uploads/61d80abe9f2c79ce084dac176c0edac7/Screenshot_2023-10-12_143233.png)


# 2180607481 / 21DTHD2

| **Function**                      | **Describe** |
|-----------------------------------|-----------------------------------------|
| **Name**                          | Library management software - Create and print library cards to easily manage staff and readers.
| **Value Statement**               | As a library manager, i want create and print library cards. This cards can help us manage library staff, readers. In addition, the card helps us manage borrowing and returning books and paying for them.
| **Acceptance Criteria**           |  Create library-specific cards upon request from admin. <br>Check for errors when entering user information into the card. <br>Manage user information, allowing information to be edited if there are changes. <br>In addition, the cards be created help keep time for employees and manage payments for readers. <br>Compile necessary statistics and report user information as required by the admin (functions such as: readers who use cards the most, employees who work the most days,etc.) |
| **Definition of Done**            | Unit Tests Passed, Acceptance Criteria Met, Code Review, Functional Tests Passed, Product Owner Accepts User Story|
| **Owner**                         | Trần Thanh Hiền - 2180607481 |
| **Iteration**                     | The Creation and Printion library cards function is expected to be implemented in the 2th part of the library management application development project. |








![Screenshot_2023-10-11_153927](/uploads/2be60435b8d5eaf5c1b1ab8432bd81b4/Screenshot_2023-10-11_153927.png)





